#include "queue.h"

/*
Function initializes the queue
input: the queue, queue size
output: none
*/
void initQueue(queue* q, unsigned int size)
{
	q->queue = new int[size]; // allocating dinemic memory for array
	q->maxSize = size;
	q->count = 0;
}

/*
Function cleans the queue
input: the queue
output: none
*/
void cleanQueue(queue* q)
{
	delete[] q->queue; // deleting memory
	q->maxSize = 0;
	q->count = 0;
}

/*
Function adds a value to the queue
input: the queue, new value
output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->count < q->maxSize) // checking that the queue is not full
	{
		q->queue[q->count] = newValue;
		q->count++;
	}
}

/*
Function removes first value from the queue and push the rest of the values 1 index backwords
input: the queue
output: the value removed (-1 if the list is empty)
*/
int dequeue(queue* q)
{
	int num = -1;

	if (q->count) // checking if the queue is not empty
	{
		num = q->queue[0];

		for (int i = 0; i < q->count; i++) // moving the values one position back
		{
			q->queue[i] = q->queue[i + 1];
		}
		q->count--;
	}

	return num;
}

/*
Function prints the queue
input: the queue
output: none
*/
void printQueue(queue* q)
{
	for (int i = 0; i < q->count; i++)
	{
		std::cout << q->queue[i] << std::endl;
	}
	std::cout << std::endl;
}
