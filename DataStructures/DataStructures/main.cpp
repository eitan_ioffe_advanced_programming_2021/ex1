#include "queue.h"
#include <iostream>

int main()
{
    queue* q = new queue;

    // Checking fucnctions
    initQueue(q, 5); // creating a new queue with 5 places
    enqueue(q, 1);
    enqueue(q, 2);
    enqueue(q, 3);
    printQueue(q);

    std::cout << dequeue(q) << std::endl;

    std::cout << dequeue(q) << std::endl;

    std::cout << dequeue(q) << std::endl;

    std::cout << "Clearing queue" << std::endl;
    cleanQueue(q);
    delete(q);

    return 0;
}
