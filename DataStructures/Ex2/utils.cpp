#include "utils.h"

#define ARR_LENGTH 10

/*
Reverses the aray given
input: the array (int), array size
output: none
*/
void reverse(int* nums, unsigned int size)
{
	unsigned int i = 0;
	stack* stk = new stack;
	// Using a stack to reverse the array
	initStack(stk);

	for (i = 0; i < size; i++) // pushing the values to the stack
	{
		push(stk, nums[i]);
	}
	for (i = 0; i < size; i++) // poping the values (LIFO) and placing them in the array
	{
		nums[i] = pop(stk);
	}
	// Deleting memory:
	cleanStack(stk);
	delete(stk);
}

/*
Gets 10 numbers from user and returns an array with those numbers reversed
input: none
outpot: the array
*/
int* reverse10()
{
	int i = 0;
	int* numArr = new int[ARR_LENGTH]; // initialing the array

	// getting 10 numbers from user and placing the in the array
	std::cout << "Enter 10 numbers:" << std::endl;
	for (i = 0; i < ARR_LENGTH; i++)
	{
		std::cout << "Enter num " << i + 1 << ": ";
		std::cin >> numArr[i];
	}

	reverse(numArr, ARR_LENGTH); // reversing the array
	return numArr;
}