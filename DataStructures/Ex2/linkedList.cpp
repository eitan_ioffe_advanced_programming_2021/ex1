#include "linkedList.h"

/*
Adds a node to the head of the linked list
input: the list, node to be added
output: none
*/
void addNode(Node** head, Node* node)
{
	node->next = (*head);
	(*head) = node;
}

/*
Removes a node from the head of the list and delets it
input: the list
output: none
*/
void removeHeadNode(Node** head)
{
	Node* curr = *head;

	if (curr)
	{
		(*head) = curr->next;
		delete(curr); // deleting memory
	}
}

/*
Deletes the linked list (recursivly)
input: the list
output: none
*/
void deleteList(Node** head)
{
	if (*head != NULL) // if list not empty
	{
		if ((*head)->next != NULL) // end condition
		{
			deleteList(&((*head)->next));
		}

		delete(*head); // freeing memory
	}
}