#ifndef LINKEDLISTH
#define LINKEDLISTH

#include <iostream>

// Link (node) struct
typedef struct Node
{
	int value;
	Node* next;
} node;

//push - insert element to the top of the stack
void addNode(Node** head, Node* node);
//pop - remove element from the top of the stack
void removeHeadNode(Node** head);
void deleteList(Node** head);

#endif