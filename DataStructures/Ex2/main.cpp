#include <iostream>
#include "stack.h"
#include "utils.h"

#define ARR_LENGTH 4

int main()
{
    int arr[ARR_LENGTH] = { 1, 2, 3, 4 };
    int* arr2 = NULL;
    std::cout << "hello world" << std::endl;

    stack* stk = new stack;
    initStack(stk);
    push(stk, 1);
    push(stk, 2);
    push(stk, 3);
    printStack(stk);
    cleanStack(stk);
    delete (stk);
    std::cout << std::endl;

    reverse(arr, ARR_LENGTH);
    for (int i = 0; i < ARR_LENGTH; i++)
    {
        std::cout << arr[i] << std::endl;
    }

    arr2 = reverse10();
    for (int i = 0; i < 10; i++)
    {
        std::cout << arr2[i] << std::endl;
    }
    delete[] arr2;

    return 0;
}
