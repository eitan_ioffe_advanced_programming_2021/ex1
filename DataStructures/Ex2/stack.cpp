#include "stack.h"

/*
Initializes the linked list with NULL
input: the stack
output: none
*/
void initStack(stack* s)
{
	s->elements = NULL;
}

/*
Deltets the linked list in the stack
input: the stack
output: none
*/
void cleanStack(stack* s)
{
	deleteList(&(s->elements));
}

/*
Adds a new node to the head of the list in the stack
input: the stack
output: none
*/
void push(stack* s, unsigned int element)
{
	// initializing a new node
	Node* n = new Node;
	n->value = element;
	n->next = NULL;
	// adding the node
	addNode(&(s->elements), n);
}

/*
Removes and deletes the node in the head of the list in the stack
input: the stack
output: the value of the node that got removed
*/
int pop(stack* s)
{
	int value = NOT_FOUND;
	if (s->elements) // Saving the value that will be poped
	{
		value = s->elements->value;
	}
	removeHeadNode(&(s->elements));
	return value;
}

/*
Print's the linked list in the stack
input: the stack
output: none
*/
void printStack(stack* s)
{
	Node* curr = s->elements;
	while (curr)
	{
		std::cout << curr->value << std::endl;
		curr = curr->next;
	}
}